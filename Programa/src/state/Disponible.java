package state;

import modelo.Cliente;

public class Disponible extends EstadoBoleto {

	@Override
	public EstadoBoleto dejarDisponible() {
		try {
			throw new Exception("Error, esta butaca ya est� disponible");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public EstadoBoleto reservar(Cliente cliente) {
		Reserva ocupada = new Reserva();
		ocupada.setCliente(cliente);
		return ocupada;
	}

	@Override
	public EstadoBoleto confirmar() {
		try {
			throw new Exception("Error, no tiene una butaca seleccionada");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return this;
	}

}
