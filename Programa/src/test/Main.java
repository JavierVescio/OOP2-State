package test;

import java.util.GregorianCalendar;

import modelo.Boleto;
import modelo.Cliente;
import modelo.Funcion;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cliente cliente = new Cliente("Siciliano", "Gustavo Hernan");
		Funcion funcion = new Funcion("La teor�a del todo", new GregorianCalendar(5,2,2016));
		
		Boleto boleto10 = funcion.verBoleto(10);
		boleto10.reservarButaca(cliente);
		boleto10.confirmar();
		
		Boleto boleto11 = funcion.verBoleto(11);
		boleto11.reservarButaca(cliente);
		boleto11.cancelarCompra();
		
		Boleto boleto12 = funcion.verBoleto(12);
		boleto12.confirmar(); //que pasa?
		boleto12.cancelarCompra(); //que pasa?
		boleto12.reservarButaca(cliente); //que pasa?
		boleto12.reservarButaca(cliente); //que pasa?
		boleto12.cancelarCompra(); //que pasa?
	}

}
